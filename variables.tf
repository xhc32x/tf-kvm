variable "libvirt_disk_path" {
  description = "path for libvirt pool"
  default     = "/home/bryi/kvm/pool1"
}

variable "ubuntu_18_img_url" {
  description = "ubuntu 18.04 image"
  #default     = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
  default = "images/focal-server-cloudimg-amd64-disk-kvm.img"
}

variable "vm_hostname" {
  type        = list(string)
  description = "vm hostname"
  default     = ["ubuntu1", "ubuntu2", "ubuntu3"]
}

variable "ssh_username" {
  description = "the ssh user to use"
  default     = "ubuntu"
}

variable "ssh_private_key" {
  description = "the private key to use"
  default     = "~/.ssh/id_rsa"
}